import java.util.Scanner;

public class DemoMetaphone3
{
	public static void main(String[] args)
	{
		// example code

		Scanner kb= new Scanner(System.in);
		String word,badword;

		// NOTE: You must set the following options before calling Encode()
		Metaphone3.SetEncodeVowels(true);
		Metaphone3.SetEncodeExact(true);

		System.out.print("Enter word: ");
		word= kb.nextLine();

		System.out.print("Enter misspelled word: ");
		badword= kb.nextLine();

		Metaphone3.SetWord(word);  // Specify the word to encode
		Metaphone3.Encode();       // Encode the word
		System.out.println(word+" => "+Metaphone3.GetMetaph());  // Display the encoded word

		Metaphone3.SetWord(badword);
		Metaphone3.Encode();
		System.out.println(badword+" => "+Metaphone3.GetMetaph());

	}
}
