/**
 * A class for timing code.
 *
 * @author  T.Sergeant
 * @version for Data Structures
 *
*/

public class Timer {
  private long startTime, stopTime;

  public Timer() { reset(); }
  public void reset() { startTime= stopTime = 0; }
  public void start() { startTime= System.currentTimeMillis(); }
  public void stop() { stopTime = System.currentTimeMillis(); }
  public long milliseconds() {
    if (stopTime < startTime) return 0;
    return stopTime -startTime;
  }
  @SuppressWarnings("unused")
  public double seconds() { return milliseconds() / 1000.0; }
  public String toString() {if (startTime==0) return "timer not started";
    if (stopTime ==0) stop();
    if (stopTime >= startTime)
      return String.format("%1.3f",(stopTime -startTime)/1000.0);
    return "invalid timer setting";  /* should never happen */
  }
}
