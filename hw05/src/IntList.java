/**
 * A simple linked list for holding ints.
 */
class IntList
{
	private IntNode head;


	// Initially we assume an empty list
	public IntList() {
			head= null;
	}


	// Display all elements in the list
	public void display()
	{
		for (IntNode p=head; p!=null; p=p.next)
			System.out.println(p.data);
	}


	// Add to beginning of list
	public void add(int newData)
	{
		head= new IntNode(newData,head);
	}


	// insert in ordered location
	public void insert(int newData)
	{
		IntNode p,q;
		if (head==null || head.data>=newData)
			head= new IntNode(newData,head);
		else {
			q= head; // make the compiler happy
			for (p=head; p!=null && newData>p.data; p=p.next)
				q= p;
			q.next= new IntNode(newData,p);
		}
	}


	// search for val in the list return reference to IntNode if found, null
	// otherwise.
	public IntNode search(int val)
	{
		IntNode p= head;
		while (p!=null && p.data!=val)
			p= p.next;
		return p;
	}


	// remove the (first) specified value from the list if it exists
	public void remove(int val)
	{
		if (head==null) return;
		IntNode q,p;

		q= null;	// make the compiler happy
		for (p=head; p!=null && p.data!=val; p=p.next)
			q= p;
		if (p==null) return;	// element not found
		if (p==head)         // deleting first element
			head= head.next;
		else                 // deleting middle/end element
			q.next= p.next;
	}
}
