/**
 * A linked list node that holds integer data and provide a couple of constructors.
 */

public class IntNode
{
	int	data;
	IntNode next;

	public IntNode(int newdata) {
		data= newdata;
	}

	public IntNode(int newdata, IntNode next) {
		data= newdata;
		this.next= next;
	}
}
