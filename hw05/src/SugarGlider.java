/**
 * A SugarGlider has a name and a wingSpan -- that's it.
 */

public class SugarGlider
{
	private String name;
	private double wingSpan;

	public SugarGlider (String name, double span)
	{
		this.name= name;
		this.wingSpan= span;
	}
}
