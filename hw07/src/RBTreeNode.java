/**
* Defines single node of a red-black tree.
*
* @author  T.Sergeant
* @version for Data Structures HW
* @see RBTree
*
*/
@SuppressWarnings("unused")
public class RBTreeNode
{
	String data;               // for Bible program
	char color;                   // (R)ed or (B)lack
	RBTreeNode left,right,parent; // pointers


	/**
	* Create a red node with specified data and NIL parent.
	*
	* @param data reference to data object of node
	* @param NIL reference to NIL node
	*
	* <p>NOTE: This implementation uses a NIL sentinel (instead of null) for
	* representation of dead-end pointers ... so a reference to NIL
	* must be supplied.</p>
	*/
	public RBTreeNode(String data, RBTreeNode NIL)
	{
		this(data,'R',NIL,NIL);
	}


	/**
	* Create a node with specified data and color and NIL parent.
	*
	* @param data reference to data object of node
	* @param color color of node
	* @param NIL reference to NIL node
	*/
	public RBTreeNode(String data, char color, RBTreeNode NIL)
	{
		this(data,color,NIL,NIL);
	}


	/**
	* Create a node with specified data, color, and parent.
	*
	* @param data reference to data object of node
	* @param color color of node
	* @param NIL reference to NIL node
	* @param parent reference to parent of node being created
	*/
	public RBTreeNode(String data, char color, RBTreeNode NIL, RBTreeNode parent)
	{
		this.data= data;
		this.color= color;
		this.parent= parent;
		left= right= NIL;
	}


	/**
	 * Display the data using its toString() method.
	 *
	 * @return String representation of the data
	 */
	@Override
	public String toString()
	{
		return data;
	}
}
