/**
 * Count the number of lines for all txt files found in the specified directory.
 *
 * @author Terry Sergeant
 * @version for DS
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FilenameFilter;

public class LineCounter {
    public static void main(String [] args) throws Exception {
        // must have 1 command-line argument
        if (args == null || args.length != 1) {
            System.out.println("Usage: java LineCounter name_of_directory");
            throw new Exception("Invalid number of arguments");
        }

        // that argument must be a directory name
        File folder = new File(args[0]);
        if (!folder.isDirectory()) {
            System.out.println("Usage: java LineCounter name_of_directory");
            throw new Exception("Argument must be a directory");
        }

        // We want to work with .txt files
        FilenameFilter txtFileFilter = (dir, name) -> name.endsWith(".txt");

        // get list of files and display their length
        Timer timer = new Timer();
        File[] files = folder.listFiles(txtFileFilter);

        timer.start();
        if (files != null) {
            for (File f : files) {
                countLines(args[0] + "/" + f.getName());
            }
        }
        timer.stop();

        System.out.println("---------------------------------------------");
        System.out.println("Files processed: " + (files==null ? 0 :files.length));
        System.out.println("Time elapsed   : " + timer);
        System.out.println("---------------------------------------------");
    }


        /**
         * Given name of a file, count the number of lines and print the result.
         */
    public static void countLines(String filename) {
        int count = 0;
        try (BufferedReader f = new BufferedReader(new FileReader(filename))) {
            while (f.readLine() != null) {
                count++;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println(filename + " has " + count + " lines");
    }
}
