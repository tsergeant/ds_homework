import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LineCounterTest {
    @Test
    void main() {
        try {
            LineCounter.main(new String[]{}); // should print usage message
            fail("Should have thrown 'Invalid Number of Arguments' exception");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}