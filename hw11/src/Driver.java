/**
 * Count the number of lines for all txt files found in the specified directory.
 *
 * @author  Terry Sergeant
 * @version for DS
 *
*/

public class Driver
{
	public static final int NUM_THREADS= 1000;
	public static final String filename= "count.txt";
	public static void main(String [] args) throws Exception {
		int i;
		Thread [] pool= new Thread[NUM_THREADS];

		for (i=0; i<NUM_THREADS; i++) {
			if (i % 2 == 0) {
				pool[i]= new Thread(new Reader(filename));
			}
			else {
				pool[i]= new Thread(new Writer(filename));
			}
			pool[i].start();
		}

		for (i=0; i<NUM_THREADS; i++) {
			pool[i].join();
		}
	}
}
