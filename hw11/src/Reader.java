/**
 * Readers reads the file and displays the results; creates it if it doesn't exist.
 *
 * @author  Terry Sergeant
 * @version for DS
 *
*/

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Reader implements Runnable {
	private final String filename;

	public Reader(String filename) {
		this.filename= filename;
	}

	public void run() {
		Thread me= Thread.currentThread();
		try {
			int num;
			Scanner fin= new Scanner(Files.newInputStream(Paths.get(filename)));
			num= fin.nextInt();
			fin.close();
			System.out.println("Reader '" + me.getName() + "' says count is: " + num);
		}
		catch(Exception e) {
			System.out.println("Reader '" + me.getName() + "' " + e);
		}
	}
}
