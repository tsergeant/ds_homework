/**
 * Writer reads the current value, adds 1 and rewrites the file.
 *
 * @author  Terry Sergeant
 * @version for DS
 *
*/

import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;

@SuppressWarnings("SpellCheckingInspection")
public class Writer implements Runnable {
	private final String filename;

	public Writer(String filename) {
		this.filename= filename;
	}

	public void run() {
		Thread me= Thread.currentThread();
		try {
			int num;
			Scanner fin= new Scanner(new FileInputStream(filename));
			num= fin.nextInt();
			fin.close();
			num++;
			PrintStream fout= new PrintStream(filename);
			fout.println(num);
			fout.close();
			System.out.println("Writer '" + me.getName() + "' incremented count to: " + num);
		}
		catch(FileNotFoundException e) {
			try {
				PrintStream fout= new PrintStream(filename);
				fout.println(1);
				fout.close();
				System.out.println("Writer '" + me.getName() + "' created file: " + filename);
			}
			catch(Exception e2) {
				System.out.println("yikes");
				System.out.println(e2.getMessage());
			}
		}
		catch(Exception e) {
			System.out.println("Writer '" + me.getName() + "' " + e);
		}
	}
}
